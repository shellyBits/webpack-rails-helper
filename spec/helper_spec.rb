require 'spec_helper'

describe Webpack::RailsHelper::Helper, type: :helper do
  let(:source_js)       { 'entry1.js' }
  let(:source_css)      { 'entry1.css' }
  let(:source2)         { 'entry2' }
  let(:source2_js)      { 'entry2.js' }
  let(:source2_css)     { 'entry2.css' }
  let(:asset_path_js)   { '/public/webpack/entry1-123abc.js' }
  let(:asset_path_css)  { '/public/webpack/entry1-123abc.css' }
  let(:asset_path2_js)  { '/public/webpack/entry2-456def.js' }
  let(:asset_path2_css) { '/public/webpack/entry2-456def.css' }

  describe 'webpack_asset_path' do
    it 'returns the asset path' do
      expect(Webpack::RailsHelper::Manifest).to receive(:asset_paths)
        .with(source_js, false)
        .and_return(asset_path_js)

      expect(helper.webpack_asset_path source_js).to eq(asset_path_js)
    end
  end

  describe 'javascript_webpack_tag' do
    it 'returns a javascript tag for an asset' do
      expect(Webpack::RailsHelper::Manifest).to receive(:asset_paths)
        .with([source_js])
        .and_return([asset_path_js])

      expect(helper.javascript_webpack_tag source_js).to eq("<script src=\"#{asset_path_js}\"></script>")
    end

    it 'returns a javascript tag for an asset, automatically adding .js extension' do
      expect(Webpack::RailsHelper::Manifest).to receive(:asset_paths)
        .with([source2_js])
        .and_return([asset_path2_js])

      expect(helper.javascript_webpack_tag source2).to eq("<script src=\"#{asset_path2_js}\"></script>")
    end

    it 'returns multiple javascript tags for multiple asset' do
      expect(Webpack::RailsHelper::Manifest).to receive(:asset_paths)
        .with([source_js, source2_js])
        .and_return([asset_path_js, asset_path2_js])

      expect(helper.javascript_webpack_tag source_js, source2).to eq(
        "<script src=\"#{asset_path_js}\"></script>" "\n" \
        "<script src=\"#{asset_path2_js}\"></script>"
      )
    end
  end

  describe 'stylesheet_webpack_tag' do
    it 'returns a stylesheet tag for an asset' do
      expect(Webpack::RailsHelper::Manifest).to receive(:asset_paths)
        .with([source_css], true)
        .and_return([asset_path_css])

      expect(helper.stylesheet_webpack_tag source_css).to eq(
        "<link rel=\"stylesheet\" media=\"screen\" href=\"#{asset_path_css}\" />")
    end

    it 'returns a stylesheet tag for an asset, automatically adding .css extension' do
      expect(Webpack::RailsHelper::Manifest).to receive(:asset_paths)
        .with([source2_css], true)
        .and_return([asset_path2_css])

      expect(helper.stylesheet_webpack_tag source2).to eq(
        "<link rel=\"stylesheet\" media=\"screen\" href=\"#{asset_path2_css}\" />")
    end

    it 'returns multiple stylesheet tags for multiple asset' do
      expect(Webpack::RailsHelper::Manifest).to receive(:asset_paths)
        .with([source_css, source2_css], true)
        .and_return([asset_path_css, asset_path2_css])

      expect(helper.stylesheet_webpack_tag source_css, source2).to eq(
        "<link rel=\"stylesheet\" media=\"screen\" href=\"#{asset_path_css}\" />" "\n" \
        "<link rel=\"stylesheet\" media=\"screen\" href=\"#{asset_path2_css}\" />"
      )
    end
  end
end
