require 'spec_helper'
require 'json'

describe "Webpack::RailsHelper::Manifest" do
  let(:manifest) do
    <<-EOF
      {
        "entry1.css": "entry1-123abc.css",
        "entry1.js": "entry1-123abc.js",
        "entry2.js": "entry2-456efg.js"
      }
    EOF
  end

  before do
    ::Rails.configuration.webpack.dev_server.host = 'client-host'
    ::Rails.configuration.webpack.dev_server.port = 2000
    ::Rails.configuration.webpack.dev_server.manifest_host = 'server-host'
    ::Rails.configuration.webpack.dev_server.manifest_port = 4000
    ::Rails.configuration.webpack.manifest_filename = "my_manifest.json"
    ::Rails.configuration.webpack.public_path = "public_path"
    ::Rails.configuration.webpack.output_dir = "manifest_output"
  end

  before :each do
    Webpack::RailsHelper::Manifest.clear_manifest
  end

  context "with dev server enabled" do
    before do
      ::Rails.configuration.webpack.dev_server.enabled = true
    end

    context "valid asset path" do
      before do
        stub_request(:get, "http://server-host:4000/public_path/my_manifest.json").to_return(body: manifest, status: 200)
      end

      it "should return single entry asset paths from the manifest" do
        expect(Webpack::RailsHelper::Manifest.asset_paths("entry2.js")).to eq("http://client-host:2000/public_path/entry2-456efg.js")
      end

      it "should return multiple entry assets paths from the manifest" do
        expect(Webpack::RailsHelper::Manifest.asset_paths([ "entry1.js", "entry1.css" ])).to eq([
          "http://client-host:2000/public_path/entry1-123abc.js",
          "http://client-host:2000/public_path/entry1-123abc.css",
        ])
      end

      it "should return empty string for optional asset" do
        expect(Webpack::RailsHelper::Manifest.asset_paths("entry2.css", true)).to eq("")
      end

      it "should error on a missing entry point" do
        expect {
          Webpack::RailsHelper::Manifest.asset_paths("herp")
        }.to raise_error(Webpack::RailsHelper::Manifest::EntryPointMissingError)
      end
    end

    context "invalid asset path" do
      before do
        stub_request(:get, "http://server-host:4000/public_path/my_manifest.json").to_raise(SocketError)
      end

      it "should error if we can't find the manifest" do
        expect {
          Webpack::RailsHelper::Manifest.asset_paths("entry1.js")
        }.to raise_error(Webpack::RailsHelper::Manifest::ManifestLoadError)
      end
    end
  end

  context "with dev server disabled" do
    before do
      ::Rails.configuration.webpack.dev_server.enabled = false
    end

    context "valid asset path" do
      before do
        allow(File).to receive(:read).with(::Rails.root.join("manifest_output/my_manifest.json")).and_return(manifest)
      end

      it "should return single entry asset paths from the manifest" do
        expect(Webpack::RailsHelper::Manifest.asset_paths("entry2.js")).to eq("/public_path/entry2-456efg.js")
      end

      it "should return empty string for optional asset" do
        expect(Webpack::RailsHelper::Manifest.asset_paths("entry2.css", true)).to eq("")
      end

      it "should error on a missing entry point" do
        expect {
          Webpack::RailsHelper::Manifest.asset_paths("herp")
        }.to raise_error(Webpack::RailsHelper::Manifest::EntryPointMissingError)
      end
    end

    context "invalid asset path" do
      before do
        expect(File).to receive(:read).with(::Rails.root.join("manifest_output/my_manifest.json")).and_raise(Errno::ENOENT)
      end

      it "should error if we can't find the manifest" do
        expect {
          Webpack::RailsHelper::Manifest.asset_paths("entry1.js")
        }.to raise_error(Webpack::RailsHelper::Manifest::ManifestLoadError)
      end
    end
  end
end
