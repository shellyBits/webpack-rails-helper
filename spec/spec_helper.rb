require 'simplecov'

SimpleCov.start 'rails'

require "rails"
require "action_controller/railtie"
require "action_view/railtie"
require "rails/test_unit/railtie"

require "rspec/rails"
require "webpack-rails-helper"
require 'webmock/rspec'

module Dummy
  class Application < Rails::Application
    config.secret_key_base = '3d1c3067fd991181148541536cc2e8b2'
    config.eager_load = false
  end
end

Rails.application.initialize!
