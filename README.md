# webpack-rails-helper

This gem integrates assets from webpack into a Ruby on Rails application. It
basically converts between the asset name and the asset name as emitted by
webpack with hashes and stuff added.


This works by using a JSON manifest file. When using together with
`webpack-dev-server`, the manifest is re-read on every call to one of the
helpers. This isn't exactly efficient but isn't much of a problem in development
mode. In production mode, the manifest is read once from file.


## Installation


  * Add this to `Gemfile`:

	```
    gem 'webpack-rails-helper'
	```

  * Run `bundle`

  * Add this to `"devDependencies"` of `package.json`:

	```
	"webpack-manifest-plugin": "^1.2.1"
	```

  * Run `yarn` (or `npm install` if you must)

  * In `webpack.config.js`, add the plugin

	```
	...
	plugins: [
		// other plugins here
		new ManifestPlugin()
	]
	...
	```

## Configuration

In `config/application.rb` or in a `config/environments/*.rb`, configure it.
Defaults are as follows:

```
    config.webpack.dev_server.host = 'localhost'
    config.webpack.dev_server.port = 3808

    # The host and port to use when fetching the manifest
    # This is helpful for e.g. docker containers, where the host and port you
    # use via the web browser is not the same as those that the containers use
    # to communicate among each other. Uses 'host' and 'port' when set to nil.
    config.webpack.dev_server.manifest_host = nil
    config.webpack.dev_server.manifest_port = nil

    config.webpack.dev_server.https = false # note - this will use OpenSSL::SSL::VERIFY_NONE
    config.webpack.dev_server.enabled = ::Rails.env.development?

    config.webpack.output_dir = 'public/webpack'
    config.webpack.public_path = 'webpack'
    config.webpack.manifest_filename = 'manifest.json'
```

## Running

It's recommended to use `foreman` or something similar to start Rails and the
webpack server during development. When using `foreman`, the `Procfile` could
look like this:

```
rails: ./bin/rails server
webpack: ./node_modules/.bin/webpack-dev-server --hot --inline --config config/webpack.config.js
```


## API

The gem provides helper methods, available in views and controllers:


  * Generic asset:

	```
    webpack_asset_path(source, optional = false)
	```

	When using this method, the full name must be specified, including the file
	extension. When `optional` is set to `true`, a non-existing asset will not
	raise an exception. This is useful when some assets are only available in
	production mode, e.g. extracted CSS.

  * Javascript tag:

	```
    javascript_webpack_tag(*names, **options)
	```

	This creates a javascript include tag, just like `javascript_include_tag`
	would, but using an asset from webpack. The `.js` extension is added
	automatically if required.


  * Stylesheet tag:

	```
    stylesheet_webpack_tag(*names, **options)
	```

	This creates a stylesheet link tag, just like `stylesheet_link_tag`
	would, but using an asset from webpack. The `.css` extension is added
	automatically if required. CSS assets are considered optional when the
	dev_server is running for reasons stated above.


# Acknowledgements

This started as a fork of <https://github.com/mipearson/webpack-rails.git>


The reason for the fork is the usage of a different webpack manifest format. The
original depends on 'stats-webpack-plugin' while this uses
'webpack-manifest-plugin'. Since the file formats are so different, it results
in an incompatible API.

This code has lived inside an application and has been extracted back out into
this gem.


# License

This gem is released under the [MIT license](LICENSE).
