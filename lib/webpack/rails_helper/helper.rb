require 'action_view'
require_relative 'manifest'

module Webpack
  module RailsHelper::Helper
    def webpack_asset_path(source, optional = false)
      return '' unless source.present?
      Webpack::RailsHelper::Manifest.asset_paths(source, optional)
    end

    def javascript_webpack_tag(*names, **options)
      js = names.map { |s| s.end_with?('.js') ? s : s + '.js' }
      sources = Webpack::RailsHelper::Manifest.asset_paths(js)
      javascript_include_tag(*sources, **options)
    end

    def stylesheet_webpack_tag(*names, **options)
      css = names.map { |s| s.end_with?('.css') ? s : s + '.css' }
      optional = Webpack::RailsHelper::Config.dev_server_enabled? || options[:optional]
      sources = Webpack::RailsHelper::Manifest.asset_paths(css, optional)
      stylesheet_link_tag(*sources, **options)
    end
  end
end
