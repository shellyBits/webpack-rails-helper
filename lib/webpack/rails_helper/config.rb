require 'uri'

class Webpack::RailsHelper::Config
  class << self
    def dev_server_enabled?
      ::Rails.configuration.webpack.dev_server.enabled
    end

    def asset_prefix
      if config_root.dev_server.enabled
        dev_server_asset_prefix
      else
        static_asset_prefix
      end
    end

    def static_manifest_path
      ::Rails.root.join(config_root.output_dir, config_root.manifest_filename)
    end

    def manifest_uri
      @manifest_uri ||= URI::Generic.build(
        scheme: config_root.dev_server.https ? 'https' : 'http',
        host: manifest_host,
        port: config_root.dev_server.manifest_port || config_root.dev_server.port,
        path: "/#{config_root.public_path}/#{config_root.manifest_filename}"
      )
    end

    private

    def static_asset_prefix
      @asset_prefix ||= "/#{config_root.public_path}/"
    end

    def dev_server_asset_prefix
      port = config_root.dev_server.port
      protocol = config_root.dev_server.https ? 'https' : 'http'

      host = config_root.dev_server.host
      host = instance_eval(&host) if host.respond_to?(:call)

      "#{protocol}://#{host}:#{port}/#{config_root.public_path}/"
    end

    def config_root
      ::Rails.configuration.webpack
    end

    def manifest_host
      host = config_root.dev_server.manifest_host || config_root.dev_server.host
      host = instance_eval(&host) if host.respond_to?(:call)
      host
    end
  end
end
