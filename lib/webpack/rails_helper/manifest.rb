require 'net/http'
require 'uri'

module Webpack
  class RailsHelper::Manifest
    class ManifestLoadError < StandardError; end
    class EntryPointMissingError < StandardError; end

    class << self
      def asset_paths(source, optional = false)
        manif = manifest
        if source.is_a? Enumerable
          source.map { |s| single_asset_path(manif, s, optional) }
        else
          single_asset_path(manif, source, optional)
        end
      end

      def load_manifest
        manifest unless Webpack::RailsHelper::Config.dev_server_enabled?
        nil
      end

      def clear_manifest
        @manifest = nil
      end

      private

      def single_asset_path(manif, source, optional)
        path = manif[source]
        if path
          Webpack::RailsHelper::Config.asset_prefix + path
        elsif !optional
          raise EntryPointMissingError, "Can't find entry point '#{source}' in webpack manifest"
        else
          ''
        end
      end

      def manifest
        if Webpack::RailsHelper::Config.dev_server_enabled?
          # Always reload from dev_server as manifest may change
          load_dev_server_manifest
        else
          @manifest ||= load_static_manifest
        end
      end

      def parse_manifest(data)
        JSON.parse(data)
      end

      def load_dev_server_manifest
        uri = Webpack::RailsHelper::Config.manifest_uri
        http = Net::HTTP.new(uri.host, uri.port)
        http.use_ssl = uri.scheme == 'https'
        http.verify_mode = OpenSSL::SSL::VERIFY_NONE
        parse_manifest(http.get(uri.path).body)
      rescue => e
        raise ManifestLoadError,
          "Could not load manifest from webpack-dev-server at #{Webpack::RailsHelper::Config.manifest_uri}: #{e}"
      end

      def load_static_manifest
        parse_manifest(File.read(Webpack::RailsHelper::Config.static_manifest_path))
      rescue => e
        raise ManifestLoadError,
          "Could not load compiled manifest from #{Webpack::RailsHelper::Config.static_manifest_path}: #{e}"
      end
    end
  end
end
