require 'rails/railtie'

require 'webpack/rails_helper/helper'

module Webpack
  class RailsHelper::Railtie < ::Rails::Railtie
    config.after_initialize do
      ActiveSupport.on_load(:action_view) do
        ActionController::Base.helper Webpack::RailsHelper::Helper
      end

      ActiveSupport.on_load :action_view do
        include Webpack::RailsHelper::Helper
      end

      if ::Rails.configuration.eager_load
        Webpack::RailsHelper::Manifest.load_manifest
        Spring.after_fork { Webpack::RailsHelper::Manifest.load_manifest } if defined?(Spring)
      end
    end

    config.webpack = ActiveSupport::OrderedOptions.new
    config.webpack.dev_server = ActiveSupport::OrderedOptions.new

    # Host & port to use when generating asset URLS in the manifest helpers in dev
    # server mode. Defaults to the requested host rather than localhost, so
    # that requests from remote hosts work.
    config.webpack.dev_server.host = proc { respond_to?(:request) ? request.host : 'localhost' }
    config.webpack.dev_server.port = 3808

    # The host and port to use when fetching the manifest
    # This is helpful for e.g. docker containers, where the host and port you
    # use via the web browser is not the same as those that the containers use
    # to communicate among each other. Uses 'host' and 'port' when set to nil.
    config.webpack.dev_server.manifest_host = nil
    config.webpack.dev_server.manifest_port = nil

    config.webpack.dev_server.https = false # note - this will use OpenSSL::SSL::VERIFY_NONE
    config.webpack.dev_server.enabled = ::Rails.env.development?

    config.webpack.output_dir = 'public/webpack'
    config.webpack.public_path = 'webpack'
    config.webpack.manifest_filename = 'manifest.json'
  end
end
