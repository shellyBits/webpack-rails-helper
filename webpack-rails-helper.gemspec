$:.push File.expand_path('../lib', __FILE__)

require 'webpack/rails_helper/version'

Gem::Specification.new do |s|
  s.name     = 'webpack-rails-helper'
  s.version  = Webpack::RailsHelper::VERSION
  s.authors  = ['Daniel Ritz']
  s.email    = ['daniel.ritz@shellybits.ch']
  s.homepage = 'http://gitlab.com/shellyBits/webpack-rails-helper'
  s.summary  = 'Helper for using webpack in Rails'
  s.license  = 'MIT'

  s.required_ruby_version = '>= 2.0.0'

  s.add_dependency 'railties', '>= 4.2'

  s.add_development_dependency 'bundler', '~> 1.12'

  s.files = Dir['lib/**/*', 'LICENSE', 'README.md']
end
