# Changelog

## 0.3.0

* API change: `webpack_asset_path()` no longer calls `asset_path()`
* stylesheet_webpack_tags() now supports the option `optional: true`

## 0.2.0

* Fix dev_server.host as Proc
* Added specs
* README fixes

## 0.1.1

First usable version
